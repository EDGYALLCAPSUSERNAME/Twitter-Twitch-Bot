import time
import webbrowser
import twitter
import pytwitcherapi

# These are retrieved from your twitter app
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_TOKEN = ''
ACCESS_TOKEN_SECRET = ''


def find_online_streams(ts, tw, online):
    streams = ts.followed_streams()
    # intialize a blank list of all the streams that are online
    # this will purge any that went offline since the last
    # iteration
    current = []
    print "Checking for streams..."
    for stream in streams:
        if stream.twitchid not in online:
            tweet_stream(tw, stream)
            current.append(stream.twitchid)
        else:
            current.append(stream.twitchid)
    return current


def tweet_stream(tw, stream):
    tweet = "{}: {} is online now at {}".format(time.strftime('%m/%d at %I:%M%p'),
                                                stream.channel.name,
                                                stream.channel.url)
    print "Tweeting stream link"
    try:
        tw.PostUpdate(tweet)
    except twitter.TwitterError, e:
        if str(e.message) == 'Status is a duplicate.':
            print "Already tweeted..."


def main():
    # Authorize with Twitch
    ts = pytwitcherapi.TwitchSession()
    ts.start_login_server()
    url = ts.get_auth_url()
    webbrowser.open(url)
    raw_input("Press enter after you authorize with Twitch\n")
    ts.shutdown_login_server()
    assert ts.authorized, "Authorization failed, please try again."
    print"Logged in as {}".format(ts.current_user.name)

    # Authorize with Twitter
    tw = twitter.Api(consumer_key=CONSUMER_KEY,
                     consumer_secret=CONSUMER_SECRET,
                     access_token_key=ACCESS_TOKEN,
                     access_token_secret=ACCESS_TOKEN_SECRET)

    # Initialize an empty online list to keep track of
    # channels already tweeted about
    online = []
    while True:
        online = find_online_streams(ts, tw, online)
        print "Sleeping..."
        time.sleep(60)


if __name__ == '__main__':
    main()
