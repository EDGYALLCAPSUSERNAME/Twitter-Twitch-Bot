Twitch Twitter Bot
==================

Setup
-----

This script requires python 2.7 the python twitter wrapper, and the pytwitcherapi.

To install the pytwitcherapi type in:

    pip install pytwitcherapi

To install the python twitter wrapper [download it here](https://code.google.com/p/python-twitter/)
and follow the instructions on the site.

You must also setup a twitter app.

Go to [apps.twitter.com](http://apps.twitter.com) and create a new app.

Fill out the form on the next page:

![twitter app form](http://i.imgur.com/9q1NO8s.png)

Then you'll be able to retrieve your keys and access tokens from the
keys and access tokens tab.

Your consumer keys will look like this:

![consumer keys](http://i.imgur.com/S3tGixg.png)

Add those to the twitter_auth_code_bot.py

Then go to the bottom of the page and find the button to generate your access
tokens.

After generating your access tokens you'll see something like this:

![access tokens](http://i.imgur.com/kDuMrAk.png)

Add those to the twitter_twitch_bot.py

Streams
-------

The channel that will get tweeted are the channels that the account
that authorizes the bot is following.

The timestamp is put on the tweets to circumvent the twitter api from not allowing
you to tweet the same exact thing twice (i.e. the channel is live, then offline, then live again in a short period).